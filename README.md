
# KS Replay Uploader ReadMe

  

Document contains information how to compile your own binary/.exe file and run it.

Program tested and works on Windows 10/11

Languages: English | Korean | Chinese

  

Additional [FAQ](https://docs.google.com/document/d/15q4fcqK4m5K-HoEt990El4KIedqswadTzQsn8Os-M5c/edit#heading=h.qzk45pn6biia) and how to use program

[Releases](https://gitlab.com/johnlake9998/ks-replay-uploader-source-code/-/releases)

# Compiling it yourself

Program is created in Go lang, using standard libraries and fyne for GUI.

You need [Golang](https://go.dev/doc/install), a terminal ([Windows - Git Bash](https://git-scm.com/downloads)), and a C compiler.

Full [instructions](https://developer.fyne.io/started/) to use fyne. Fyne requires a C-complier, and the fyne website have clear instructions.

(Note all $ here denote a new line in terminal. Do not enter it.)

Check if go is properly installed: $ go

If there is no error message, then proceed. In terminal, change the directory to where you git cloned or downloaded the source code. Initiate a go module, and give it a name. Tidy will make hte module file correctly reference fyne as a dependency.

  

$ cd Path_to_main.go

$ go mod init MODULE_NAME

$ go mod tidy

If both are installed correctly, you compile by typing:

$ fyne package

OR

$ go build -ldflags "-s -w -H=windowsgui" -trimpath

  

fyne package is the fyne integrated compiling
go build is the regular go version of creating a binary.
The flags are there to remove debugging.


# Files
NotoEN.ttf - Noto sans font for English. Source: Google fonts

NotoNONEN.ttf - Noto sans font for CJK (Chinese, Japanse, Korea). Source: Google fonts

Icon.png - Kerrigan art

bundled.go - contains data for compiling to include the icon and fonts informations into main.go

main.go - source code for the app. Go lang. Uses Fyne for GUI.
