package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"image/color"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"sort"
	"strings"
	"time"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/data/binding"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/layout"
	"fyne.io/fyne/v2/theme"
	"fyne.io/fyne/v2/widget"
)

const ver = "1.07" // Version value
const cConfig = "4"
const configFile = "config.json"
const logErrors = "logErrors.log"
const ksDIR = "KerriganSurvival"
const updateWeb = "https://gitlab.com/api/v4/projects/36380951/releases"
const webserver = "https://replay.kerrigansurvival.com/upload"
const webRelease = "https://gitlab.com/johnlake9998/ks-replay-uploader-source-code/-/releases"

var currentOS = runtime.GOOS
var lang = 0                      // 0 - EN | 1 - KR | 2 - CN
var gvlastTime int64 = 1667684503 // Nov 5 epoch time (valid replays)
var gvuProfile = ""
var gvuRecord = ""
var gvCancel = false
var activateCounter = false
var magicHead = []byte{0x4d, 0x50, 0x51, 0x1b, 0x00, 0x02, 0x00, 0x00, 0x00, 0x04}
var myApp fyne.App
var myWindow fyne.Window
var isSubmitRunning = false
var updateStatus = false // start false
var autoUpFalse *bool
var autoUploadbool = binding.BindBool(autoUpFalse) //start false
var accountHandles = []string{}
var playerHandle = []string{}

type version struct {
	TagName string `json:"tag_name"`
}

// Struct to hold translation
type i18nStruct struct {
	on                     string // Activate
	off                    string // Deactivate
	tray                   string // Minimize to sys tray
	browse                 string // Browse for file path
	submit                 string // Submit replay, one time
	file, exit, en, cn, kr string // File submenu and browse
	view, text             string // View submenu
	help, update, about    string // Help Submenu
	version                string // Version
	ks2                    string // KS2 name title
	replay                 string // Replays
	done                   string // Done!
	submitted              string // submitted!
	entryE                 string // entry title label
	entryM                 string // entry message
	fileEval               string //
	cCancel                string // cancel button
	sStatus                string // Server Status Error
	noUpdate               string // No updates
	autoUpload             string //Automatically upload
	claim                  string // /claim credits
	pHLabel                string //pHandle label
	clip                   string //
	requiredUpdate         string //
}

// Defines the strings for translation: EN | KR | CN
var i18n = []i18nStruct{
	i18nStruct{
		on:             "   Activate",
		off:            "Deactivate",
		tray:           "Minimize to System Tray",
		browse:         "Browse",
		submit:         "Submit",
		file:           "File",
		view:           "View",
		text:           "Language",
		exit:           "Quit",
		help:           "Help",
		update:         "Check for Updates",
		about:          "About",
		version:        "ver.",
		ks2:            "Kerrigan Survival 2 Replay Uploader | Account Folder:",
		replay:         "Replays",
		done:           "Done!",
		submitted:      " submitted!",
		entryE:         "Entry Error",
		entryM:         "Please find and select your Starcraft II/Accounts folder",
		fileEval:       "Kerrigan Survival",
		cCancel:        "Cancel",
		sStatus:        "Please retry later. Internet issues",
		noUpdate:       "No updates at this time",
		autoUpload:     "Automatically Upload:",
		claim:          "",
		pHLabel:        "Player Handle",
		clip:           "Click to copy to Clipboard",
		requiredUpdate: "A new update is required. Please download",
		en:             "English",
		cn:             "Mandarin (中文)",
		kr:             "Korean (한국어)",
	},
	i18nStruct{
		on:             "활성화",
		off:            "비활성화",
		tray:           "시스템 트레이로 최소화",
		browse:         "검색",
		submit:         "등록",
		file:           "파일",
		view:           "보기",
		text:           "언어",
		exit:           "나가기",
		help:           "도움말",
		update:         "업데이트 확인",
		about:          "정보",
		version:        "버전",
		ks2:            "케리건 살아남기 2 리플레이 업로더 | 계정 폴더:",
		replay:         "리플레이",
		done:           "좋아요!",
		submitted:      "제출된",
		entryE:         "입력 오류",
		entryM:         "StarCraft II/Accounts 폴더를 찾아 선택해주세요",
		fileEval:       "Kerrigan Survival", //Should be English
		cCancel:        "취소",
		sStatus:        "나중에 다시 시도해주세요. 인터넷 문제",
		noUpdate:       "업데이트가 없습니다",
		autoUpload:     "자동 업로드:",
		claim:          "",
		pHLabel:        "플레이어 핸들",
		clip:           "클릭하여 클립보드에 복사",
		requiredUpdate: "새 업데이트가 필요합니다. 다운로드하십시오.",
		en:             "English",
		cn:             "Mandarin (中文)",
		kr:             "Korean (한국어)",
	},
	i18nStruct{
		on:             "开启",
		off:            "关闭",
		tray:           "托盘",
		browse:         "浏览",
		submit:         "提交",
		file:           "文件",
		view:           "观看",
		text:           "文本",
		exit:           "退出",
		help:           "帮助",
		update:         "检查更新",
		about:          "关于",
		version:        "版本",
		ks2:            "凯瑞甘生存2 最新版 | 路径:",
		replay:         "录像",
		done:           "完成!",
		submitted:      " 提交!",
		entryE:         "输入错误",
		entryM:         "请找到并选择《星际争霸二》根目录",
		fileEval:       "凯瑞甘生存", // Discord can't read CN
		cCancel:        "取消",
		sStatus:        "请稍后重试! 互联网问题",
		noUpdate:       "现在没有更新",
		autoUpload:     "自动上传:",
		claim:          "",
		pHLabel:        "玩家ID",
		clip:           "单击以复制到剪贴板",
		requiredUpdate: "需要新的更新. 请下载.",
		en:             "English",
		cn:             "Mandarin (中文)",
		kr:             "Korean (한국어)",
	},
}

// Struct for config file
type storedValueStruct struct {
	LastTime       int64  //epoch time
	SelectLang     int    //en|kr|cn
	SavedFolder    string //replay folder
	VConfig        string //version control
	AutoUploadFlag bool   //auto upload flag
}

// Struct for sorting replays by time
type validReplay struct {
	Path string
	Time int64
}

// Initial stored values to create config.json
var storedValue = []storedValueStruct{
	storedValueStruct{
		LastTime:       gvlastTime,
		SelectLang:     lang,
		SavedFolder:    gvuProfile,
		VConfig:        cConfig,
		AutoUploadFlag: false,
	},
}

// Var to store replays in list
var replayString = binding.BindStringList(
	&[]string{},
)

// Setting up a custom theme for fyne GUI
type myTheme struct{}

var _ fyne.Theme = (*myTheme)(nil)

// Back ground color
func (m myTheme) Color(name fyne.ThemeColorName, variant fyne.ThemeVariant) color.Color {
	return theme.DefaultTheme().Color(name, variant)
}
func (m myTheme) Icon(name fyne.ThemeIconName) fyne.Resource {
	return theme.DefaultTheme().Icon(name)
}

// Custom Font - Asian character support
func (m myTheme) Font(name fyne.TextStyle) fyne.Resource {
	return resourceNotoNONENTtf
}

// Default text size
func (m myTheme) Size(name fyne.ThemeSizeName) float32 {
	return theme.DefaultTheme().Size(name)
}

// ///////////////
// Init Function//
// ///////////////
func init() {
	//Not sure if this calls correct mac directory
	//Determine OS
	if runtime.GOOS != "windows" {
		if _, err := os.Stat(filepath.FromSlash(os.Getenv("LOGIN") + "Library/Application_Support/StarCraft II/Accounts")); !os.IsNotExist(err) {
			gvuProfile = filepath.FromSlash(os.Getenv("LOGIN") + "Library/Application_Support/StarCraft II/Accounts")
			gvuRecord = filepath.FromSlash(os.Getenv("LOGIN") + "Library/Application_Support")
		}
	} else {
		gvuRecord = filepath.FromSlash(os.Getenv("PROGRAMDATA"))
		if _, err := os.Stat(filepath.FromSlash(os.Getenv("USERPROFILE") + "/Documents/StarCraft II/Accounts")); err == nil {
			gvuProfile = filepath.FromSlash(os.Getenv("USERPROFILE") + "/Documents/StarCraft II/Accounts")
		} else {
			gvuProfile = filepath.FromSlash(os.Getenv("USERPROFILE") + "/OneDrive/Documents/StarCraft II/Accounts")
		}
	}

	folderDIR()
	f, err := os.OpenFile(gvuRecord+"/"+ksDIR+"/"+logErrors, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Println(ver+" ver. "+currentOS+" "+" logFile: Could not access logErrors.txt", err)
	}
	//defer to close when you're done with it, not because you think it's idiomatic!
	defer f.Close()
	//set output of logs to f
	log.SetOutput(f)
	autoUploadbool.Set(false)
	initialconfig() // creates or reads from config file
	checkUpdate()
	accountID() //reads accounts
}

// //////////////////
// GUI and Main App//
// //////////////////
func main() {
	//create your file with desired read/write permissions
	f, err := os.OpenFile(gvuRecord+"/"+ksDIR+"/"+logErrors, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0644)
	if err != nil {
		log.Println(ver+" ver. "+currentOS+" "+" logFile: Could not access logErrors.txt", err)
	}
	//defer to close when you're done with it, not because you think it's idiomatic!
	defer f.Close()
	//set output of logs to f
	log.SetOutput(f)
	//test case
	myApp = app.New()
	myApp.Settings().SetTheme(&myTheme{})
	//App name
	myWindow := myApp.NewWindow(i18n[lang].ks2)
	//Icon
	mainIcon := fyne.Resource(resourceIconPng)
	myWindow.SetIcon(mainIcon)
	//Sets window size and prevent rescaling
	myWindow.Resize(fyne.NewSize(500, 500))
	myWindow.SetFixedSize(true)

	///////////////////////
	//Title or name plate//
	///////////////////////

	//Kerrigan Survival Replay Uploader text
	title := widget.NewLabel(i18n[lang].ks2)
	title.Move(fyne.NewPos(0, 5))
	title.TextStyle = fyne.TextStyle{Bold: true}
	cTitle := container.NewWithoutLayout(title)

	//Entry form
	//Binds entry from to gv gvuProfile
	fpBind := binding.NewString()
	fpBind.Set(gvuProfile)
	folderPath := widget.NewEntryWithData(fpBind)
	//Reposition + Size
	folderPath.Resize(fyne.NewSize(395, 40))
	folderPath.Move(fyne.NewPos(5, 0))
	folderPath.Disable()

	//Infinite Progress Bar
	//Active only when toggled on
	iProgress := widget.NewProgressBarInfinite()
	iProgress.Resize(fyne.NewSize(480, 30))
	iProgress.Move(fyne.NewPos(5, 0))
	iProgress.Hide()

	//Browse dialog open
	bBrowse := widget.NewButton(i18n[lang].browse, func() {
		chooseDirectory(myWindow, func(s string) {
			gvuProfile = s
			fpBind.Set(gvuProfile)
			updateConfig()
		})
	})
	bBrowse.Resize(fyne.NewSize(85, 30))
	bBrowse.Move(fyne.NewPos(405, 5))
	cPath := container.NewWithoutLayout(folderPath, iProgress, bBrowse)

	//Submit button - one time instance
	bSubmit := widget.NewButton(i18n[lang].submit,
		func() {
			if gvuProfile != "" {
				gvCancel = false
				if updateStatus != false {
					dialog.ShowInformation(i18n[lang].update, i18n[lang].requiredUpdate, myWindow)
					openbrowser(webRelease)
					return
				}
				if isSubmitRunning != true {
					go watcherActivate(iProgress, bBrowse, folderPath)
					isSubmitRunning = true
				}
			} else {
				dialog.ShowInformation(i18n[lang].entryE, i18n[lang].entryM, myWindow)
			}
		})
	bSubmit.Resize(fyne.NewSize(95, 35))
	bSubmit.Move(fyne.NewPos(145, 5))
	bCancel := widget.NewButton(i18n[lang].cCancel,
		func() {
			gvCancel = true
			isSubmitRunning = false
		})
	bCancel.Resize(fyne.NewSize(95, 35))
	bCancel.Move(fyne.NewPos(235, 5))
	cSubmit := container.NewWithoutLayout(bSubmit, bCancel)

	/////////////////
	//Player Handle//
	/////////////////

	//Selects player handle
	pHandle := binding.NewString()
	pHandle.Set(i18n[lang].claim)
	accountsSelect := widget.NewSelect(playerHandle,
		func(s string) {
			pHandle.Set(i18n[lang].claim + s)
		})
	accountsSelect.Resize(fyne.NewSize(150, 40))
	accountsSelect.Move(fyne.NewPos(5, 40))
	//pHandle Label
	pHandleLabel := widget.NewLabel(i18n[lang].pHLabel)
	pHandleLabel.Move(fyne.NewPos(5, 10))
	//Button fake label
	clipLabel := widget.NewLabelWithData(pHandle)
	clipLabel.Move(fyne.NewPos(190, 40))
	//Button to copy paste /claim credits for discord
	pHandleButton := widget.NewButtonWithIcon("", theme.ContentCopyIcon(),
		func() {
			clippy, err := pHandle.Get()
			if err != nil {
				log.Println("Could not copy player handle", err)
			}
			myWindow.Clipboard().SetContent(clippy)
		})
	pHandleButton.Alignment = widget.ButtonAlignLeading
	pHandleButton.Resize(fyne.NewSize(320, 40))
	pHandleButton.Move(fyne.NewPos(165, 40))
	//pHandle Label
	copyB := widget.NewLabel(i18n[lang].clip)
	copyB.Move(fyne.NewPos(175, 10))

	cID := container.NewWithoutLayout(pHandleLabel, copyB, clipLabel, accountsSelect, pHandleButton)

	////////
	//List//
	////////

	// List databind == automatically updates as it is populated from watcherActivate function
	replayList := widget.NewListWithData(replayString,
		func() fyne.CanvasObject {
			return widget.NewLabel("template")
		},
		func(i binding.DataItem, o fyne.CanvasObject) {
			o.(*widget.Label).Bind(i.(binding.String))
		})
	replayList.Resize(fyne.NewSize(450, 200))
	replayList.Move(fyne.NewPos(20, 60))
	cList := container.NewWithoutLayout(replayList)

	//Version indicator
	tVersion := widget.NewLabel(i18n[lang].version)
	tVnum := widget.NewLabel(ver)
	tVersion.Move(fyne.NewPos(0, 220))
	tVnum.Move(fyne.NewPos(35, 220))

	/////////////
	//Auto Init//
	/////////////
	//Future problem. container.NewWithoutLayout does not allow check to work. Could try slider
	/*autoUploadCheck := widget.NewCheck(i18n[lang].autoUpload, func(b bool) {
		if b == true {
			log.Println("True")
		} else {
			log.Println("False")
		}
	})
	autoUploadCheck.Move(fyne.NewPos(0, 200))*/
	cVersion := container.NewWithoutLayout(tVersion, tVnum)
	//cAuto := container.New

	////////////////////
	//File Menu System//
	////////////////////

	//File items
	//Opens up browse dialog
	fileBrowse := fyne.NewMenuItem(i18n[lang].browse, func() {

		chooseDirectory(myWindow, func(s string) {
			gvuProfile = s
			fpBind.Set(gvuProfile)
			updateConfig()
		})
	},
	)
	fileMenu := fyne.NewMenu(i18n[lang].file, fileBrowse)

	//View items
	viewEN := fyne.NewMenuItem(i18n[lang].en, func() {
		lang = 0
		updateConfig()
		myApp.Quit()
	})
	viewKR := fyne.NewMenuItem(i18n[lang].kr, func() {
		lang = 1
		updateConfig()
		myApp.Quit()
	})
	viewCN := fyne.NewMenuItem(i18n[lang].cn, func() {
		lang = 2
		updateConfig()
		myApp.Quit()
	})
	viewMenu := fyne.NewMenu(i18n[lang].view, viewEN, viewKR, viewCN)

	//Help Menu
	helpUpdate := fyne.NewMenuItem(i18n[lang].update, func() {
		if updateStatus != false {
			dialog.ShowInformation(i18n[lang].update, i18n[lang].requiredUpdate, myWindow)
			openbrowser(webRelease)
		} else {
			dialog.ShowInformation(i18n[lang].update, i18n[lang].noUpdate, myWindow)
		}

	})
	helpAbout := fyne.NewMenuItem(i18n[lang].about, func() {
		dialog.ShowInformation("About", "Written in Go lang. All rights reserved. Copyright 2022. KS2 Dev", myWindow)
	})
	helpMenu := fyne.NewMenu(i18n[lang].help, helpUpdate, helpAbout)

	menu := fyne.NewMainMenu(fileMenu, viewMenu, helpMenu)
	myWindow.SetMainMenu(menu)
	myWindow.SetContent(container.New(layout.NewVBoxLayout(), cTitle, cPath, cSubmit, cID, cList, cVersion))
	myWindow.ShowAndRun()
}

// //////////////////////
// Supporting Functions//
// //////////////////////
// Runs at start before main everytime in func init
// Checks for config.json file
func initialconfig() {
	if _, err := os.Stat(gvuRecord + "/" + ksDIR + "/" + configFile); err == nil {
		//Read stored data from json file
		jsonFile, err := os.Open(gvuRecord + "/" + ksDIR + "/" + configFile)
		if err == nil {
			defer jsonFile.Close()
			byteValue, _ := ioutil.ReadAll(jsonFile)
			var data storedValueStruct
			err = json.Unmarshal(byteValue, &data)
			if data.VConfig == cConfig {
				err = json.Unmarshal(byteValue, &data)
				gvlastTime = data.LastTime // Last time program ran
				lang = data.SelectLang     // Selected language
				if _, err := os.Stat(data.SavedFolder); err != nil {
					if os.IsNotExist(err) {
						gvuProfile = ""
					} else {
						gvuProfile = data.SavedFolder
					}
				}
				autoUploadbool.Set(data.AutoUploadFlag) // Is auto upload on start checked?
				return
			} else {
				os.Remove(configFile)
				activateCounter = true
				config := storedValueStruct{
					LastTime:       gvlastTime,
					SelectLang:     lang,
					SavedFolder:    gvuProfile,
					VConfig:        cConfig,
					AutoUploadFlag: false,
				}
				file, _ := json.MarshalIndent(config, "", " ")
				_ = ioutil.WriteFile(gvuRecord+"/"+ksDIR+"/"+configFile, file, 0644)
			}
		} else if errors.Is(err, os.ErrNotExist) {
			activateCounter = true
			config := storedValueStruct{
				LastTime:       gvlastTime,
				SelectLang:     lang,
				SavedFolder:    gvuProfile,
				VConfig:        cConfig,
				AutoUploadFlag: false,
			}
			//Create config file
			file, _ := json.MarshalIndent(config, "", " ")
			_ = ioutil.WriteFile(gvuRecord+"/"+ksDIR+"/"+configFile, file, 0644)
		}
	}
}

// Stores current gv into config file.
func updateConfig() {
	autoBool, err := autoUploadbool.Get()
	if err != nil {
		log.Println("Could not get auto upload boolean", err)
	}
	config := storedValueStruct{
		LastTime:       gvlastTime,
		SelectLang:     lang,
		SavedFolder:    gvuProfile,
		VConfig:        cConfig,
		AutoUploadFlag: autoBool,
	}
	file, _ := json.MarshalIndent(config, "", " ")
	_ = ioutil.WriteFile(gvuRecord+"/"+ksDIR+"/"+configFile, file, 0644)

}

//Should run everytime program uploads replays
//Records last time program ran for replay comparisons

// Chooses directory to search through
// Initial default SC2 installation folder
func chooseDirectory(w fyne.Window, cb func(string)) {
	dialog.ShowFolderOpen(func(dir fyne.ListableURI, err error) {
		save_dir := gvuProfile
		if err != nil {
			log.Println(ver+" ver. "+currentOS+" "+" ver. chooseDirectory: Could not set save_dir to gvuProfile", err, w)
			return
		}
		if dir != nil {
			save_dir = dir.Path() // here value of save_dir shall be updated!
		}
		cb(save_dir)
	}, w)
}

// File parser
// Recursively search through selected directory and subdirectories
func watcherActivate(p *widget.ProgressBarInfinite, b *widget.Button, f *widget.Entry) {
	if isSubmitRunning != true {
		return
	}
	f.Hide()
	b.Hide()
	p.Show()
	var validReplays []validReplay
	populateReplays(&validReplays)
	//sorts the replays by epoch time
	sort.Slice(validReplays[:], func(i, j int) bool {
		return validReplays[i].Time < validReplays[j].Time
	})
	//Webserver
	webClient := &http.Client{}
	for _, replay := range validReplays {
		if gvCancel == true {
			break
		}
		webUpload(replay, *webClient)
	}
	//Display done when finished
	if gvCancel != true {
		replayString.Append(i18n[lang].done)
	} else {
		replayString.Append(i18n[lang].cCancel)
	}
	updateConfig()
	p.Hide()
	b.Show()
	f.Show()
	isSubmitRunning = false
}

func populateReplays(arrayList *[]validReplay) {
	err := filepath.Walk(gvuProfile,
		func(path string, info os.FileInfo, err error) error {
			//Cancel Function
			if gvCancel == true {
				return nil
			}
			//File path error
			if err != nil {
				log.Println(ver+" ver. "+currentOS+" "+" watcherActivate: File path issue", err, path)
				return err
			}
			//Quick metadata comparison for file
			if filepath.Ext(path) != ".SC2Replay" {
				return nil
			}
			metaD, err := os.Stat(path)
			if err != nil {
				log.Println(ver+" ver. "+currentOS+" "+" watcherActivate: Could not grab meta data", err)
				return nil
			}

			if gvlastTime > metaD.ModTime().Unix() || metaD.ModTime().Unix() > time.Now().Unix() {
				return nil
			}
			if metaD.Size() > 3145728 {
				return nil
			}
			replayStruct := validReplay{Path: path, Time: metaD.ModTime().Unix()}
			*arrayList = append(*arrayList, replayStruct)
			return nil
		})
	if err != nil {
		log.Println(ver+" ver. "+currentOS+" "+" Could not walk through dir", err)
	}
}

func folderDIR() {
	path := gvuRecord + "/" + ksDIR
	if _, err := os.Stat(path); errors.Is(err, os.ErrNotExist) {
		err := os.Mkdir(path, os.ModePerm)
		if err != nil {
			log.Println(ver+" ver. "+currentOS+" "+" folderDIR: Could not create directory", err)
		}
	}
}

func openbrowser(url string) {
	var err error
	switch runtime.GOOS {
	case "linux":
		err = exec.Command("xdg-open", url).Start()
	case "windows":
		err = exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	case "darwin":
		err = exec.Command("open", url).Start()
	default:
		err = fmt.Errorf("unsupported platform")
	}
	if err != nil {
		log.Fatal(err)
	}
}

// Checks for update on gitlab
func checkUpdate() {
	url := updateWeb
	//HTTP Get
	res, err := http.Get(url)
	if err != nil {
		log.Println(err)
	}
	defer res.Body.Close()
	//Read the body
	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		log.Println(err)
	}

	var data []version
	//Grab/Slice the tag_name for version control
	err = json.Unmarshal(body, &data)
	if err != nil {
		panic(err)
	}
	//If version differ, then update
	if data[0].TagName != ("v" + ver) {
		updateStatus = true
	}
}

// Compiles list of SC2 accounts
func accountID() {

	filename, err := os.ReadDir(gvuProfile)
	if err != nil {
		log.Println("Could not find account folders", err)
	}
	var playerID []string
	var accountID []string
	// Loop over files.
	for outputIndex := range filename {
		outputFileHere := filename[outputIndex]

		// Get name of file.
		outputNameHere := outputFileHere.Name()
		accountID = append(accountID, outputNameHere)
	}
	accountHandles = accountID
	for i := 0; i < len(accountHandles); i++ {
		handlename, err := os.ReadDir(gvuProfile + "/" + accountHandles[i])
		if err != nil {
			log.Println("Could not find player handles", err)
		}
		// Loop over files.
		for outputIndex := range handlename {
			outputFileHere := handlename[outputIndex]

			// Get name of file.
			outputNameHere := outputFileHere.Name()
			if strings.Contains(outputNameHere, "-S2-") {
				playerID = append(playerID, outputNameHere)
			}
			playerHandle = playerID
		}
	}
}

func webUpload(replay validReplay, webServer http.Client) {
	// replay is the individual struct for where we are in loop
	if gvCancel == true {
		updateConfig()
		return
	}
	replayFile, err := os.Open(replay.Path)
	defer replayFile.Close()
	if err != nil {
		log.Println("Could not open path to file", err)
		return
	}
	if gvlastTime > replay.Time {
		return
	}
	//Magic header key
	byteSlice := make([]byte, 10)
	replayFile.Read(byteSlice)
	if err != nil {
		log.Println(ver+" ver. "+currentOS+" "+" ver. watcher Activate: Could not create byte slice", err, byteSlice)
		return
	}
	//Byte comparisons
	compare := bytes.Compare(magicHead, byteSlice)
	if compare != 0 {
		return
	}
	replayFile.Seek(0, 0)
	webPost, err := http.NewRequest("POST", webserver, replayFile)
	webPost.Header.Add("User-Agent", "kerrigan-survival-uploader/"+ver)
	resp, err := webServer.Do(webPost)
	if err != nil || resp.StatusCode != 200 {
		replayString.Append(i18n[lang].sStatus)
		gvCancel = true
		gvlastTime = replay.Time - 1
		updateConfig()
		return
	}
	defer resp.Body.Close()

	//Output replay submitted to list
	replayString.Append(filepath.Base(replay.Path) + i18n[lang].submitted)
	//Sets time to last evalualted file creation)
	gvlastTime = replay.Time + 1
}
